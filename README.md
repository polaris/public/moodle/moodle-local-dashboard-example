# Polaris Dashboard moodle

## Install moodle Plugin
- unzip the repository to `/local/polaris_dashboard` at your moodle web directory

## Install new Database table `polaris_dashboard``
- go to XMLDB-Editor at the moodle-administration and create the SQL for creating the database

## To build Polaris JavaScript bundle.js

```bash
cd js
npm install
npm run build
```