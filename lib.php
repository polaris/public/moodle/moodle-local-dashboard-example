<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin functions for the local_[pluginname] plugin.
 *
 * @package   local_polaris_dashboard
 * @copyright 2023, Center für Lehr- und Lernservice (CLS), RWTH Aachen University
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 defined('MOODLE_INTERNAL') || die();

 use core\hook\output\before_html_attributes;


 require_once($CFG->dirroot . "/local/polaris_dashboard/locallib.php");

/**
 * Function to add a navigation node to the course navigation if one is given.
 * This adjustment is overwritten by the navigation setup of Moodle version 4+.
 * Code to adjust the navigation in version 4+ is given below.
 *
 * @param global_navigation $navigation The global navigation object to get the course navigation from
 */
function local_polaris_dashboard_extend_navigation(global_navigation $navigation){
    global $COURSE;

    // Get navigation of course
    $course_nav = $navigation->find($COURSE->id, navigation_node::TYPE_COURSE);

    local_polaris_dashboard_extend_arbitrary_navigation($course_nav);
    
}
