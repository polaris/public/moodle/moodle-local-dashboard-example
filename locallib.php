<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin functions for the local_[pluginname] plugin.
 *
 * @package   local_polaris_dashboard
 * @copyright 2023, Center für Lehr- und Lernservice (CLS), RWTH Aachen University
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function local_polaris_dashboard_extend_arbitrary_navigation($navigation){
    global $COURSE;
    
    // Create navigation node
    // create(identifier, url/action, type, shorttext, key, pix:icon)
    $polaris_dashbaord_node = navigation_node::create(
        get_string('pluginname', 'local_polaris_dashboard'),
        new moodle_url('/local/polaris_dashboard/index.php', array('id' => $COURSE->id)),
        navigation_node::TYPE_CUSTOM,
        null,
        'polaris_dashboard',
        new pix_icon('i/report', '')
    );

    // If 'grades' is part of the course navigation, add the navigation node before it
    if(in_array('grades', $navigation->get_children_key_list())){
        $navigation->add_node($polaris_dashbaord_node, 'grades');
        return;
    }

    // If not, try to add it before the last setting node

    // Get all section nodes in the navigation and sort them by key (ascending)
    $course_settings_nav_nodes = $navigation->children->type(navigation_node::TYPE_SETTING);

    // Check if children are given; If none are given, add the navigation node at the end
    $insert_before = null;
    if(count($course_settings_nav_nodes) > 0){
        // Get last element
        $insert_before = array_values($course_settings_nav_nodes)[count($course_settings_nav_nodes) - 1]->key;
    }
    $navigation->add_node($polaris_dashbaord_node, $insert_before);
}