const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require("path");

module.exports = {
  entry: "./src/app.js",
  output: {
    path: path.resolve(__dirname, "../amd/src/"),
    filename: "bundle-lazy.js",
  },
  devtool: 'source-map',

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
            plugins: ["@babel/plugin-proposal-object-rest-spread"],
          },
        },
      },

      {
        test: /\.(scss|css)$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },

      {
        test: /\.(gif|jpeg|jpg|png|svg|webp)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 8192,
              name: "[path]/[name].[ext]",
              include: [/images/],
            },
          },
        ],
      },

      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path]/[name].[ext]",
              include: [/fonts/],
            },
          },
        ],
      },
    ],
  },

  plugins: [
  ],
  externals: {
    "core/modal_factory": {
      amd: "core/modal_factory"
    }
  },
  devServer: {
    static: {
      directory: path.join(__dirname, ""),
    },
    host: "0.0.0.0",
    port: 8005,
    allowedHosts: "all",
    client: {
      overlay: {
        errors: true,
      },
      progress: true,
    },
  },
};
