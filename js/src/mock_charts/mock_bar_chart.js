import { BarChartWidget, ChartJSWidget } from "@polaris/dashboard-sdk";

const dict = {
    "accessed": 98,
    "started": 7,
    "graded": 7,
    "submitted": 7,
    "answered": 38,
    "leftUnanswered": 4
}
const data = Object.keys(dict).map(key => ({column1: key, column2: dict[key]}));

export const mockBarChart = { "barchart-widget" :
        new BarChartWidget(
            "Kursnutzung",
            "Darstellung des Fortschritts der Teilnehmer anhand der Nutzung der Lerninhalte",
            data,
            {
                showLegend: true,
                xAxisLabel: "Status",
                yAxisLabel: "Anzahl"
            })
}


const chartjs_data = {datasets: [{data: Object.keys(dict).map(key => dict[key])}], labels: Object.keys(dict)};

export function mockChartJSBarChart(onShowDesc)
{
    return { "chartjs-barchart-widget" :
            new ChartJSWidget(
                "Kursnutzung",
                "Darstellung des Fortschritts der Teilnehmer anhand der Nutzung der Lerninhalte",
                chartjs_data,
                {
                    chartjs_options: {
                        scales: {
                            x: {
                                title: {
                                    display: true,
                                    text: "Status"
                                }
                            },
                            y: {
                                title: {
                                    display: true,
                                    text: "Anzahl"
                                }
                            }
                        },
                        plugins: {
                            legend: {
                                display: false,
                            }
                        },
                    },
                    onShowDesc: onShowDesc
                }
            )
    }
}
