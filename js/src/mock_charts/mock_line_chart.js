import { LineChartWidget } from "@polaris/dashboard-sdk";

const data = [
    {column1: 0, column2: 80},
    {column1: 10, column2: 60},
    {column1: 20, column2: 70},
    {column1: 30, column2: 20},
    {column1: 40, column2: 0},
    {column1: 50, column2: 60},
    {column1: 60, column2: 70},
    {column1: 70, column2: 80},
    {column1: 80, column2: 76},
    {column1: 90, column2: 60},
    {column1: 100, column2: 70},
]

export function mockLineChart(onShowDesc) {
    return { "line-chart-widget" :
            new LineChartWidget(
                "Kursnutzung",
                "Durchschnittliche Kursnutzung nach Blöcken",
                data,
                {
                    showLegend: false,
                    xAxisLabel: "Blöcke",
                    yAxisLabel: "Teilnahme",
                    onShowDesc: onShowDesc
                })
    }
}