<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin functions for the local_[pluginname] plugin.
 *
 * @package   local_polaris_dashboard
 * @copyright 2023, Center für Lehr- und Lernservice (CLS), RWTH Aachen University
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Polaris Dashboard';
$string['manage'] = 'Settings Polaris Dashboard';
$string['engines_students'] = 'Students Analytics Engines';
$string['eninges_students_desc'] = 'Comma separated list of students analytics engines';
$string['engines_teachers'] = 'Teachers Analytics Engines';
$string['eninges_teachers_desc'] = 'Comma separated list of teachers analytics engines';
$string['token_api_url'] = 'Token API URL';
$string['token_api_url_desc'] = 'URL to the Polaris Dashboard Token API';
$string['auth_provider_token'] = 'Authorization Bearer Token of the provider';
$string['auth_provider_token_desc'] = 'Authorization Bearer Token of the Polaris Provider without leading `Bearer `';
$string['url_result_store_desc'] = 'URL to the Polaris result store';
$string['url_result_store'] = 'URL to the Polaris result store';
