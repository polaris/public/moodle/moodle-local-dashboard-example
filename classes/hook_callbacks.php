<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_polaris_dashboard
 * @copyright 2023, Center für Lehr- und Lernservice (CLS), RWTH Aachen University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_polaris_dashboard;

defined('MOODLE_INTERNAL') || die();

class hook_callbacks {
    /**
     * Workaround to bypass the course navigation override by Moodle version 4+.
     * Is called after the override.
     * An "_extend_course_navigation" function would not allow to add the navigation node in front of the grade-entry.
     * As the local_ladashboard_extend_navigation function works on earlier versions,
     * the branch is checked and the function is aborted if an earlier version than 4 is used.
     */
    static function local_polaris_dashboard_before_html_attributes($attributes) {
        global $PAGE, $CFG;

        if ($PAGE->context->contextlevel != CONTEXT_COURSE || ((int)$CFG->branch) < 400) {
            return $attributes;
        }

        local_polaris_dashboard_extend_arbitrary_navigation($PAGE->secondarynav);

        return $attributes;
    }
}
