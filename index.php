<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin functions for the local_polaris_dashboard plugin.
 *
 * @package   local_polaris_dashboard
 * @copyright 2023, Center für Lehr- und Lernservice (CLS), RWTH Aachen University
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__.'/../../config.php');
require_once($CFG->dirroot . '/local/polaris_dashboard/components/classes/external/polaris_api.php');
//use local_polaris_dashboard\components\classes\external\polaris_api_external;

// Get parameters from URL
$courseid = optional_param('id', 0, PARAM_INT);// Course ID

// Set page parameters
$params = array();

if($courseid !== 0){
    $params['id'] = $courseid;
}

// Create moodle url with parameters
$url = new moodle_url("/local/polaris_dashboard/index.php", $params);
// Set url
$PAGE->set_url('/local/polaris_dashboard/index.php', array('id' => $courseid));
// Set page layout to url as it is a report wrapped in a local-plugin
$PAGE->set_pagelayout('report');

// Get course details
$course = null;
if ($courseid) {
    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
    require_login($course);
    $context = context_course::instance($course->id);
} else {
    require_login();
    $context = context_system::instance();
    $PAGE->set_context($context);
}

$PAGE->set_title($COURSE->fullname . ' - ' . get_string('pluginname', 'local_polaris_dashboard'));
$PAGE->set_heading(get_string('pluginname', 'local_polaris_dashboard'));

//read from database, if user already has a token for the dashboard
$tokenDbRecord = $DB->get_record('polaris_dashboard', array('user_moodle_id' => (int) $USER->id));
//check, if token is still valid and not expired
//convert datetime to timestamp
$givenTokenExipresAsTimestamp = $tokenDbRecord !== false ? strtotime($tokenDbRecord->expires) : 0;
if($tokenDbRecord !== false && $tokenDbRecord->token !== '' && $givenTokenExipresAsTimestamp > time()){
    $token = $tokenDbRecord->token;
}else{
    $externalAPI = new polaris_api_external();
    //if not, get a new token from the polaris api
    $token = $externalAPI->get_polaris_token();
    $token = $token['token'];
    $tokenExpires = time() + 60*60*24; //token expires after 24 hours
    //format tokenExpires to datetime
    $tokenExpires = date('Y-m-d H:i:s', $tokenExpires);

    //save token in database
    if($tokenDbRecord === false){
        $tokenDbRecord = new stdClass();
        $tokenDbRecord->user_moodle_id = (int) $USER->id;
        $tokenDbRecord->token = $token;
        $tokenDbRecord->expires = $tokenExpires;
        $DB->insert_record('polaris_dashboard', $tokenDbRecord, false);
    }else{
        $tokenDbRecord->token = $token;
        $tokenDbRecord->expires = $tokenExpires;
        $DB->update_record('polaris_dashboard', $tokenDbRecord);
    }
}

//get the result store url from the config
$config = get_config('local_polaris_dashboard');
$resultStoreUrl = $config->url_result_store;


echo $OUTPUT->header();

$jslink = new moodle_url('/local/polaris_dashboard/amd/src/bundle-lazy.js');
$PAGE->requires->js($jslink);
//$PAGE->requires->js_call_amd('polaris_dashboard/bundle-lazy', 'onInitDashboard');

echo <<<EOT
<script>
    let token = '$token';
    let url = '$resultStoreUrl';
</script>
EOT;
echo <<<EOT
<button id="toggle-sidebar-btn" class="btn btn-primary mx-2">☰</button>
<a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
  <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap" /></svg>
  <span class="fs-4">RUB Dashboard</span>
</a>
</header>

<div id="example-section">

<div class="wrapper">
  <div class="sidebar-hidden" id="sidebar">
    <div class="dropzone-remove">
      <div>Drop here to remove</div>
    </div>
    <div class="available-widgets" id="available-widgets"></div>
  </div>
  <div class="grid">
    <div class="grid-stack"></div>
  </div>
</div>
</div>

<!-- Widget Description Modal -->
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog">
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-body" id="modal-content-body"></div>
  </div>
</div>
</div>

<!-- Error Modal -->
<div class="modal fade" id="myErrorModal" role="dialog">
<div class="modal-dialog">
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-body" id="error-modal-content-body"></div>
  </div>
</div>
</div>
EOT;

echo $OUTPUT->footer();