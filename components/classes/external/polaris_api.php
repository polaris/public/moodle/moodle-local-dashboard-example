<?php

//namespace local_polaris_dashboard\components\classes\external;

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/config.php');
require_once("$CFG->libdir/externallib.php");

class polaris_api_external extends external_api {

    //get token from polaris api
    public static function get_polaris_token_parameters() {
        return new external_function_parameters(
            array()
        );
    }

    public static function get_polaris_token() {

        global $USER;

        //read authorization data from config.php
        $config = get_config('local_polaris_dashboard');
        $authorization = $config->auth_provider_token;
        $polaris_token_url = $config->token_api_url;
        
        $engines = explode(',' , $config->teachers_analytics_engines);
        $engines=preg_replace('/\s+/', '', $engines);

        $headers = array(
            'Authorization: Basic ' . $authorization,
            'Content-Type: application/json'
        );

        //add user-id and the analytics engines from the config to the body request
        $body = json_encode(array(
            //names of engines should be as an array
            'engines' => $engines,
            'context_id' => $USER->id //TODO: should be email(?) of moodle user
        ));

        // Set up the cURL request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $polaris_token_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Send the request and get the response
        $response = curl_exec($ch);

        // Close the cURL request
        curl_close($ch);

        // Process the response and return the token
        $response_data = json_decode($response, true);
        $token = $response_data['token'];

        return array(
            'token' => $token
        );

    }
}
