<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Admin settings for the local_polaris_dashboard plugin.
 *
 * @package   local_polaris_dashboard
 * @copyright 2023, Center für Lehr- und Lernservice (CLS), RWTH Aachen University
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $ADMIN->add('localplugins', new admin_category('local_polaris_dashboard_settings', new lang_string('pluginname', 'local_polaris_dashboard')));
    $settingspage = new admin_settingpage('manageploarisdashboard', new lang_string('manage', 'local_polaris_dashboard'));

    if ($ADMIN->fulltree) {

        //URL to the Polaris Dashboard Token API
        $settingspage->add(new admin_setting_configtext(
            'local_polaris_dashboard/token_api_url',
            new lang_string('token_api_url', 'local_polaris_dashboard'),
            new lang_string('token_api_url_desc', 'local_polaris_dashboard'),
            ''
        ));

        //URL to the Polaris Result Store to get the data for the dashboard
        $settingspage->add(new admin_setting_configtext(
            'local_polaris_dashboard/url_result_store',
            new lang_string('url_result_store', 'local_polaris_dashboard'),
            new lang_string('url_result_store_desc', 'local_polaris_dashboard'),
            ''
        ));

        //Authorization Bearer Token of the Polaris Provider
        $settingspage->add(new admin_setting_configtext(
            'local_polaris_dashboard/auth_provider_token',
            new lang_string('auth_provider_token', 'local_polaris_dashboard'),
            new lang_string('auth_provider_token_desc', 'local_polaris_dashboard'),
            ''
        ));

        // Students analytics engines
        $settingspage->add(new admin_setting_configtext(
            'local_polaris_dashboard/students_analytics_engines',
            new lang_string('engines_students', 'local_polaris_dashboard'),
            new lang_string('eninges_students_desc', 'local_polaris_dashboard'),
            ''
        ));

        // Teachers analytics engines
        $settingspage->add(new admin_setting_configtext(
            'local_polaris_dashboard/teachers_analytics_engines',
            new lang_string('engines_teachers', 'local_polaris_dashboard'),
            new lang_string('eninges_teachers_desc', 'local_polaris_dashboard'),
            ''
        ));
    }

    $ADMIN->add('localplugins', $settingspage);
}